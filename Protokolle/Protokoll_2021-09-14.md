
# Pikos Python Kurs 14.09.2021



# Pikos IDLE
```
[iason@medea ~]$ python
Python 3.9.7 (default, Aug 31 2021, 13:28:12) 
[GCC 11.1.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> satz = "happy coding"
>>> values = [ord(buchstabe) for buchstabe in satz]
>>> values
[104, 97, 112, 112, 121, 32, 99, 111, 100, 105, 110, 103]
>>> for buchstabe in satz:
... 
KeyboardInterrupt
>>> liste = []
>>> for buchstabe in satz:
...     liste = liste + [ord(buchstabe)]
... 
>>> liste
[104, 97, 112, 112, 121, 32, 99, 111, 100, 105, 110, 103]
>>> for buchstabe in satz:
...     print(ord(buchstabe))
... 
104
97
112
112
121
32
99
111
100
105
110
103
>>> for buchstabe in satz:
...     print(ord(buchstabe), end=" ~~~ ")
... 
104 ~~~ 97 ~~~ 112 ~~~ 112 ~~~ 121 ~~~ 32 ~~~ 99 ~~~ 111 ~~~ 100 ~~~ 105 ~~~ 110 ~~~print("uae")
uae
>>> print("uae")
uae
>>> for buchstabe in satz:
...     print(buchstabe)
...     print()
... 
h

a

p

p

y

 

c

o

d

i

n

g

>>> for buchstabe in satz:
...     print(ord(buchstabe), end=" ")
... 
104 97 112 112 121 32 99 111 100 105 110 103 >>> # LIST COMPREHENSION
>>> liste
[104, 97, 112, 112, 121, 32, 99, 111, 100, 105, 110, 103]
>>> stephaniesliste = [68, 101, 114, 32, 114, 111, 116, 101, 32, 70, 117, 99, 104, 115, 32, 115, 112, 114, 105, 110, 103, 116, 32, 252, 98, 101, 114, 32, 100, 101, 110, 32, 102, 97, 117, 108, 101, 110, 32, 98, 114, 97, 117, 110, 101, 110, 32, 72, 117, 110, 100, 46]
>>> stephaniesliste
[68, 101, 114, 32, 114, 111, 116, 101, 32, 70, 117, 99, 104, 115, 32, 115, 112, 114, 105, 110, 103, 116, 32, 252, 98, 101, 114, 32, 100, 101, 110, 32, 102, 97, 117, 108, 101, 110, 32, 98, 114, 97, 117, 110, 101, 110, 32, 72, 117, 110, 100, 46]
>>> for z in stephaniesliste:
...     print(chr(z))
... 
D
e
r
 
r
o
t
e
 
F
u
c
h
s
 
s
p
r
i
n
g
t
 
ü
b
e
r
 
d
e
n
 
f
a
u
l
e
n
 
b
r
a
u
n
e
n
 
H
u
n
d
.
>>> sylvia = """71
... 117
... 116
... 101
... 110
... 32
... 84
... 97
... 103"""
>>> s2 = sylvia.split()
>>> s2
['71', '117', '116', '101', '110', '32', '84', '97', '103']
>>> for stringzahl in s2:
...     print(chr(int(stringzahl)))
... 
G
u
t
e
n
 
T
a
g
>>> liste1 = [5, 6, 7, 8]
>>> liste2 = [] 
>>> for zahl in liste1:
...     liste2 = liste2 +       l * 10]
  File "<stdin>", line 2
    liste2 = liste2 + 	l * 10]
                             ^
SyntaxError: unmatched ']'
>>> 
```
Ab hier gehe ich kurz aus der Python-Shell raus, in mein „normales“ Terminal, von wo aus ich meinen Computer steuern kann.
Das erkennt eins daran, dass nicht mehr „>>>“ am Anfang steht, sondern 
iason[mein Benutzername]@medea[der Name meines Computers] ~[Das Zeichen informiert mich darüber, dass ich im Home-Verzeichnis bin.]
```
[iason@medea ~]$ python Desktop/regenbogenflagge.py 
[iason@medea ~]$ python Desktop/regenbogenflagge.py 
```
Hier starte ich wieder die Python-Shell:
```
[iason@medea ~]$ python
Python 3.9.7 (default, Aug 31 2021, 13:28:12) 
[GCC 11.1.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> for i in (1,2,3):
...     
KeyboardInterrupt
>>> from turtle import *
>>> for i in (1,2,3):
...     fd(100)
...     rt(120)
... 
>>> for dings in ("red", "yellow", "blue"): 
...     color(dings)
...     fd(100)
...     rt(120)
... 
Traceback (most recent call last):
  File "<stdin>", line 2, in <module>
  File "<string>", line 5, in color
turtle.Terminator
>>> for dings in ("red", "yellow", "blue"): 
...     pencolor(dings)
...     fd(100)
...     rt(120)
... 
>>> for farbe in ("red", "yellow", "blue"):
...     pencolor(farbe)
...     for i in (1, 2, 3):
...             fd(100)
...             rt(120)
...     rt(120)
... 
Traceback (most recent call last):
  File "<stdin>", line 2, in <module>
  File "<string>", line 5, in pencolor
turtle.Terminator
>>> fd(100)
>>> for farbe in ("red", "yellow", "blue"):
...     pencolor(farbe)
...     fd(100)
... 
Traceback (most recent call last):
  File "<stdin>", line 2, in <module>
  File "<string>", line 5, in pencolor
turtle.Terminator
>>> >>> for farbe in ("red", "yellow", "blue"):
  File "<stdin>", line 1
    >>> for farbe in ("red", "yellow", "blue"):
    ^
SyntaxError: invalid syntax
>>> ...     pencolor(farbe)
  File "<stdin>", line 1
    ...     pencolor(farbe)
            ^
SyntaxError: invalid syntax
>>> ...     for i in (1, 2, 3):
  File "<stdin>", line 1
    ...     for i in (1, 2, 3):
            ^
SyntaxError: invalid syntax
>>> ...             fd(100)
  File "<stdin>", line 1
    ...             fd(100)
                    ^
SyntaxError: invalid syntax
>>> ...             rt(120)
  File "<stdin>", line 1
    ...             rt(120)
                    ^
SyntaxError: invalid syntax
>>> ...     rt(120)
  File "<stdin>", line 1
    ...     rt(120)
            ^
SyntaxError: invalid syntax
>>> ... 
Ellipsis
>>> for farbe in ("red", "yellow", "blue")@
  File "<stdin>", line 1
    for farbe in ("red", "yellow", "blue")@
                                           ^
SyntaxError: invalid syntax
>>> for farbe in ("red", "yellow", "blue"):
...     pencolor(farbe)
...     for i in (1, 2, 3):
...             fd(100)
...             rt(120)
... 
>>> for farbe in ("red", "yellow", "blue"):
...     pencolor(farbe)
...     for i in (1, 2, 3):
...             fd(100)
...             rt(120)
...     rt(120)
... 
Traceback (most recent call last):
  File "<stdin>", line 2, in <module>
  File "<string>", line 5, in pencolor
turtle.Terminator
>>> for farbe in ("red", "yellow", "blue"):
...     pencolor(farbe)
...     for i in (1, 2, 3):
...             fd(100)
...             rt(120)
...     rt(120)
... 
>>> 
[iason@medea ~]$ python Desktop/regenbogenflagge.py 
```